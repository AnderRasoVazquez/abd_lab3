import java.util.Random;
import java.util.Scanner;

/**
 * 
 */

/**
 *
 */
public class Main {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// Data.getInstance().login(server, port, user, pass)
		Scanner sc = new Scanner(System.in);
		System.out.print("\n0 - Ander\n1 - Guzmán\n\nElige tu usario -> ");
		int usuario = sc.nextInt();
		sc.close();
		switch (usuario) {
		case 0:
			paraServerAnder();
			break;
		case 1:
			paraServerGuzman();
			break;
		default:
			System.out.println("Usuario no válido. Saliendo...");
			break;
		}
	}
	
	private static void paraServerAnder() {
		Data.getInstance().login("10.109.162.113", "8306", "auditorAB", "");
		
		for(int i = 1; i<=1000; i++) {
			int aCode = generarRandom(100);
			int bCode = generarRandom(100);
			int aValue = generarRandom(1000);
			int bValue = generarRandom(1000);
			Data.getInstance().executeQuery("insert into auditingAB.tableA (aCode, aValue) values (" + aCode + ", " + aValue + ")");
			Data.getInstance().executeQuery("insert into auditingAB.tableB (bCode, bValue) values (" + bCode + ", " + bValue + ")");
		}
		Data.getInstance().selectQuery("select count(*) from auditingAB.tableA "
				+ "inner join auditingAB.tableB "
				+ "on aCode=bCode");
		Data.getInstance().selectQuery("select count(*) from auditingAB.tableB "
				+ "inner join auditingAB.tableA "
				+ "on aValue=bValue");

		System.out.println("Ejecucion terminada.");
	}
	
	private static void paraServerGuzman() {
		/*
		 *  CD -> c
		 *  AB -> b
		 */	
		int numInsert = 10000;
		
		System.out.println("Has elegido Guzman");
		Data.getInstance().login("10.109.236.101", "8306", "auditorCD", "");
		System.out.println("Generando " + numInsert + " INSERT aleatorios.");
		int percent = 0;
		int lastPercent = 0;
		System.out.print("Progreso: " + lastPercent + "%");
		for (int i=0; i<=numInsert; i++) {
			Data.getInstance().executeQuery("INSERT INTO auditingCD.tableC (cCode, cValue) VALUES(" + Integer.toString(generarRandom(10)) + ", " + Integer.toString(generarRandom(1000)) + ")");
			Data.getInstance().executeQuery("INSERT INTO auditingCD.tableD (dCode, dValue) VALUES(" + Integer.toString(generarRandom(10)) + ", " + Integer.toString(generarRandom(1000)) + ")");
			percent = (i*100)/numInsert;
			if (percent != lastPercent) {
				lastPercent = percent;
				System.out.print("\rProgreso: " + lastPercent + "% ");
			}
		}
		System.out.println("\nFin de la ejecución. Saliendo...");

	}
	
	/**
	 * Genera numeros random desde el 1 hasta pNum
	 * @param pNum
	 * @return
	 */
	private static int generarRandom(int pNum) {
		Random rn = new Random();
		return rn.nextInt(pNum) + 1;
	}

}
